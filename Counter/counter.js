
     // set inital value to zero
let count = 0;
// select value and buttons
const value = document.querySelector("#value");
const btns = document.querySelectorAll(".btn");

btns.forEach(function (btn) { //forEach() method calls a function once for each element in an array
  btn.addEventListener("click", function (e) {
    const styles = e.currentTarget.classList;// e = event object for example mouse event,currentTarget = Get the element whose event listeners triggered a specific event, classList=returns the class name(s) of an element,


    if (styles.contains("decrease")) {
      count--;
    } else if (styles.contains("increase")) {
      count++;
    } else {
      count = 0;
    }

    if (count > 0) {
      value.style.color = "green";
    }
    if (count < 0) {
      value.style.color = "red";
    }
    if (count === 0) {
      value.style.color = "#222";
    }
    value.textContent = count;
  });
});

